export const HEADER = `
    <div class="retour" hidden>
        <a href=""><button id="retourAccueil" title="Retour à l'accueil" href="">&nbsp;<svg class="icon"><use xlink:href="static/icons.svg#chevron-left"/></svg>&nbsp;</button></a>
    </div>

    <div class="titre" id="titre">
        <h1>$TITRE_H1</h1>
    </div>

    <div class="aide" hidden>
        <a class="question" title="Questions"><svg class="icon"><use xlink:href="static/icons.svg#question-circle"/></svg></a>
    </div>
`;

export const APP_INDEX = `
    <section class="accueil">

        <div id="menuAccueil">
            <!-- <button id="appChrono" title="Chrono"><svg class="icon"><use xlink:href="static/icons.svg#stopwatch"/></svg></button> -->

            <button id="appApnee" title="Apnée"><svg class="icon icon-aida"><use xlink:href="static/icons.svg#aida"/></svg></button>
            <button id="appCoherCard" title="Cohérence cardiaque"><svg class="icon"><use xlink:href="static/icons.svg#equals"/></svg></button>
            <button id="appPrana" title="Pranayama"><svg class="icon"><use xlink:href="static/icons.svg#wind"/></svg></button>
            <button id="appCarre" title="Carré"><svg class="icon"><use xlink:href="static/icons.svg#carre"/></svg></button>
            <button id="appEntrainement" title="Entraînements"><svg class="icon"><use xlink:href="static/icons.svg#heartbeat"/></svg></button>
            <button id="appArbre" title="Arbre Qi Gong"><svg class="icon"><use xlink:href="static/icons.svg#tree"/></svg></button>

            <!-- <button id="appCuisson" title="Cuissons"><svg class="icon"><use xlink:href="static/icons.svg#utensils"/></svg></button> -->
            <!-- <button id="appYoupiMatin" title="Youpi matin"><svg class="icon"><use xlink:href="static/icons.svg#sun"/></svg></button> -->
            <!-- <button id="appPerso" title="Pomodoro"><svg class="icon"><use xlink:href="static/icons.svg#************** TROUVER UNE ICONE PERSO **************"/></svg></button> -->
            <!-- <button id="appPomodoro" title="Pomodoro"><svg class="icon"><use xlink:href="static/icons.svg#************** TROUVER UNE TOMATE **************"/></svg></button> -->
        </div>

    </section>
`;

export const APP_ENTRAINEMENT = `
    <div class="goodTimes">

        <div id="listeExercices" class="listeExercices"></div>
            
        <div id="affichTempsTotal" class="tempsTotal" hidden>
            Temps total : <span id="tempsTotal"></span>
        </div>
                            
        <div id="intro" class="hidden intro">
            <span id="decompte"></span>
        </div>
        
        <div id="affichSignal" class="tempsEncours" hidden>
            <span id="nomSignal"></span>
            <br>
            <span id="tempsSignal"></span>
        </div>

        <button id="boutonDepart" class="chronoBouton" title="Départ"><svg class="icon"><use xlink:href="static/icons.svg#play"/></svg></button>
        <button id="boutonPause" title="Pause" class="hidden chronoBouton"><svg class="icon"><use xlink:href="static/icons.svg#pause"/></svg></button>
        <button id="boutonReprise" title="Reprise" class="hidden chronoBouton"><svg class="icon"><use xlink:href="static/icons.svg#play"/></svg></button>
        <button id="boutonRecommencer" title="Recommencer" class="hidden chronoBouton"><svg class="icon"><use xlink:href="static/icons.svg#undo-alt"/></svg></button>

        <div id="blocReglages" hidden>
            <form id="formReglagesEnsemble">
                <label for="formDecompte">Décompte avant départ&nbsp;:</label>
                <span style="white-space:nowrap">
                    <input id="formDecompte" class="formSigDuree" type="number" min="0" max="20" name="base" value="">"
                </span>

                <br><br>

                <div><label>Affichage :</label><br>
                    <label for="formChronoPositif">Temps&nbsp;écoulé</label>
                    <input type="radio" id="formChronoPositif" name="compteARebours" value="chrono">
                    <label for="formChronoRebours">Temps&nbsp;restant</label>
                    <input type="radio" id="formChronoRebours" name="compteARebours" value="rebours">
                </div>
                
                <div class="formSignaux" id="formSignaux"></div>
                
                <button id="valideReglagesBouton" class="chronoBouton" title="Enregistrer"><svg class="icon"><use xlink:href="static/icons.svg#save"/></svg></button>

                <button id="bontonReinitialiser" hidden class="chronoBouton" type="button" title="Réinitialiser"><svg class="icon"><use xlink:href="static/icons.svg#broom"/></svg></button>

            </form>
        </div>
            
        <button id="reglagesBouton" class="chronoBouton" title="Réglages"><svg class="icon"><use xlink:href="static/icons.svg#wrench"/></svg></button>

    </div>
`;

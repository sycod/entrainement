# Good Times

> This website is a progressive web application **originally designed for French freedivers**, but might be useful to others. Perhaps.

Good Times est un **gestionnaire de chronomètres / comptes à rebours** aussi simple et intuitif que possible.
Le terme *chronomètre* est employé indifféremment que le temps soit compté ou décompté (compte à rebours).

Créé à l'origine pour des entraînements à l'apnée, nécessitant un **avertissement sonore doux et régulier du temps écoulé sans sortir de sa bulle mentale**, l'application a connu plusieurs évolutions jusqu'à permettre des **chronomètres complètement personnalisables** pour s'adapter à plusieurs types d'entraînements.

Le site est une PWA (*Progressive Web App*), c'est-à-dire qu'elle est **accessible en ligne** sur n'importe quel navigateur et **hors connexion si installée sur votre appareil avec le vanigateur Chrome**.

Les **données** sont **enregistrées dans le cacheet le local storage** du navigateur.

## Fonctionnalités

- Utiliser des **applications de chronomètres préprogrammées** (cf. liste ci-après) ;
- Permettre la personnalisation de certaines applications (précision au cas par cas ci-après) ;
- Pouvoir ajuster des **durées et signaux sonores différents** pour chaque chronomètre, jusqu'à **36 signaux différents** par application ;
- (en développement) Ajouter des **chronomètres entièrement personnalisés**.

## Applications incluses

Chronomètre simple
: Un chrono tout ce qu'il y a de plus banal, que l'on peut mettre en pause et relancer.
Il peut toutefois se paramétrer à volonté pour toute sorte d'exercice !
**Paramètres modifiables** : aucun, il a su rester simple.

Apnée
: Avertissement doux du temps écoulé toutes les minutes.
Possibilité de signaler ses contractions via un bouton. Si tel est le cas, il y aura en fin d'exercice un affichage du temps total d'apnée et du temps d'apparition de chaque contraction, la première étant mise en valeur.
**Paramètres modifiables** : décompte avant exercice ;  signaux sonores désactivables.

Cohérence cardiaque (5')
: Une inspiration de 5" puis une expiration de 5" *(soit 10" par cycle pour 6 cycles / min)*, le tout pendant 5'.
Avertissements visuel et sonore pour les 2 phases du cycle.
**Paramètres modifiables** : décompte avant exercice ; signaux sonores désactivables individuellement.

Pranayama
: Un cycle respiratoire propre au travail du Prana.
Le ratio **préconfiguré de 1:4:2:0 avec une base de 10"**, ce qui donne :
• 10" d'inspiration,
• 40" d'apnée poumons pleins,
• 20" d'expiration
• et pas d'apnée poumons vides.
L'exercice se répète jusqu'à l'arrêt par l'utilisateur.
**Paramètres modifiables** : décompte avant exercice ; affichage temps écoulé / temps restant ; base de temps (1 à 60" maximum) ; ratios (1 à 10 maximum) ; sons désactivables individuellement.

Carré d'apnée
: Exercice type d'entraînement à l'apnée statique, servant de base d'échauffement :
**Chaque cycle comporte 4 phases** : inspiration, apnée poumons pleins, expiration, apnée poumons vides.
L'exercice, sous forme de **pyramide**, est le suivant :
• 3 cycles de 5" par phase ;
• 3 cycles de 10" par phase ;
• 3 cycles de 15" par phase ;
• 3 cycles de 10" par phase ;
• 3 cycles de 5" par phase.

Entraînement physique d'exemple (30', modifiable)
: 3 cycles de 10' chacun qui s'enchaînent.
Pour chaque cycle : alternance 45" exercice / 30" récup (1'15") sur 8 exercices.
**Paramètres modifiables** : décompte avant exercice ; affichage temps écoulé / temps restant ; pour chaque exercice, on peut modifier libellé / durée / signal sonore.
    > Ce chronomètre constitue un exemple de chronomètre totalement personnalisable.
    Il est pré-réglé sur l'exercice suivant, pour lequel on privilégiera lenteur et amplitude :
    • 45" d'exercice : course sur place
    → 30" de récupération
    • 45" d'exercice : gainage abdominal
    → 30" de récupération
    • 45" d'exercice : squatts
    → 30" de récupération
    • 45" d'exercice : gainage lombaire
    → 30" de récupération
    • 45" d'exercice : équilibre / poirier
    → 30" de récupération
    • 45" d'exercice : gainage latéral droit
    → 30" de récupération
    • 45" d'exercice : pompes
    → 30" de récupération
    • 45" d'exercice : gainage latéral gauche
    → 30" de récupération

Arbre du Qi Gong (30' max)
: Avertissements du temps écoulé toutes les 5' et affichage du temps écoulé.
**Paramètres modifiables** : décompte avant exercice ; signaux sonores désactivables individuellement.

## Notes de versions

v0.1 : fonctionnalités minimales et code sale
: **Applications fonctionnelles** : chronomètre, apnée, cohérence cardiaque, pranayama, arbre Qi Gong, entraînement, activation / désactivation des sons, réglage du décompte d'avant exercice.
Configuration initiale dans **init.json**.
Conception **responsive** mobile / bureau.

v0.2 : améliorations en cours
: Bouton de **réinitialisation** (configuration de départ).
**Vérification des inputs** de réglages.

v1.0 : PWA « offline first »
: **PWA** fonctionnelle.

v1.0.1 : ajout page
: Ajout des **entraînements carrés** pour l'apnée
Suppression de l'accès au chrono simple depuis la page d'accueil.

v1.1.0 : corrections architecture et bugs
: **Architecture MVC**
Correction architecture et bugs

### En développement

NEXT : corrections architecture et bugs
: **architecture MVC** Chrono / Signal avec config par défaut dans **config.json**
refaire les **liens** pour tout
BUG pranayama : ne sauvegarde pas
mise à jour des **sons par défaut** de toutes les applications
→ màj readme (*architecture*, *en développement*, *applications incluses*, *notes de version*)

NEXT : pieds-de-page et manuels
: Rédaction des **manuels** propres à chaque application.
1 manuel par page, fenêtre modale (popin simple, refermable avec clic extérieur)
►►► Cf. notes ##Manuel + mettre manuels dans Readme

NEXT : gestion affichage
: centralisation de l'**affichage dans une fonction dédiée**
séparation moteur et affichage
améliorer l'affichage paysage mobile
menu de l'index : taille boutons selon nombre applications (4 de +)

NEXT : données externalisées
: regroupement de **toutes les données de départ dans init.json**
passage de toutes applications en **gestion de données JSON / localStorage**

NEXT : améliorations diverses
: **chrono** = entraînement vide
ajout de l'apnée poumons vides pour le **Pranayama** (ratio initial : 0)
création d'un **ensemble personnalisé** (avec données par défaut dans init.json)

NEXT : métronome
: ajout **métronome** avec **visuels** et **sons**

NEXT : sons
: enregistrement des **sons pour l'apnée** (nombre de minutes écoulées)
idem pour l'**arbre Qi Gong**
nettoyage audio → tout en OGG Opus

PEUT-ÊTRE UN JOUR : améliorations UI + tests design
: ajout **progressbar** sur chronos avec durée définie (cercle)
**réglages glisser - déposer** pour changer l'ordre des signaux
signaux visuels divers (animation changement couleur écran ou autre)
cercle de progression : <https://www.youtube.com/watch?v=t7eHSAXW718> et <https://css-tricks.com/almanac/properties/s/stroke-dashoffset/> avec animation en x secondes : <https://codepen.io/chriscoyier/pen/bGyoz>.
amélioration SCSS : <https://stackoverflow.com/questions/41635618/how-to-extend-css-class-with-another-style> (ici, notamment pour les boutons avec classe de départ à étendre parfois)
et là : <http://www.sass-lang.com/documentation/at-rules/extend#extends-or-mixins>

## Problèmes courants

Problèmes courants
: ***Les icônes des boutons ne s'affichent pas*** → mettez à jour votre navigateur ! Et oubliez Internet Explorer, le monde s'en portera bien mieux.
: Il peut y avoir des ***problèmes de cache suite à des modifications*** sur certaines applications. Pour réinitialiser, vous pouvez supprimer le cache et recharger la page.

## Architecture

``` txt
audio/
    ∟ tous les fichiers audio
data/
    ∟ init.json
JS/
    ∟ chrono.js
    ∟ signal.js
static/
    ∟ global.css
    ∟ good_times.png
    ∟ icons.svg
    ∟ templates.js
views/
    ∟ entrainement.js
.gitignore
.htaccess
good_times.webmanifest
index.html
package.json
README.md (→ vous êtes ici)
sw.js
```

## Licence

Copyleft : Good Times est libre et doit le rester, de même que ses modifications éventuelles.

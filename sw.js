// Si besoin d'importer
// self.importScripts('data/xxxxxxx.js');

// Version du cache à utiliser
const cacheName = "goodtimes-v1.1.0";

// Liste des fichiers à mettre en cache (tableau)
const fichiersApp = [
  "/audio/atari2600_bip.mp3",
  "/audio/atari2600_crash.mp3",
  "/audio/atari2600_kirby.mp3",
  "/audio/atari2600_saut.mp3",
  "/audio/cloche_aigue.mp3",
  "/audio/cloche_medium.mp3",
  "/audio/gong_court.opus",
  "/audio/gong_jap_temple_antiq_court.opus",
  "/audio/gong_jap_temple_antiq.mp3",
  "/audio/gong_jap_temple_court.opus",
  "/audio/gong_jap_temple.mp3",
  "/audio/gong.mp3",
  "/audio/JL_expi.mp3",
  "/audio/JL_inspi.mp3",
  "/audio/POC_et_voila.mp3",
  "/audio/POC_laser.mp3",
  "/audio/POC_ouais.mp3",
  "/data/init.json",
  "/JS/chrono.js",
  "/JS/entrainement.js",
  "/JS/signal.js",
  "/static/global.css",
  "/static/good_times.png",
  "/static/icons.svg",
  "/static/templates.js",
  "/views/entrainementView.js",
  "/views/indexView.js",
  "/404.html",
  "/apnee.html",
  "/arbre.html",
  "/carre.html",
  "/chrono.html",
  "/coherence-cardiaque.html",
  "/cuisson.html",
  "/exos-matin.html",
  "/index.html",
  "/pomodoro.html",
  "/pranayama.html",
  "/good_times.webmanifest"
];

// Installation du SW : ajout au cache
self.addEventListener('install', e => {
  e.waitUntil(caches.open(cacheName).then(cache => cache.addAll(fichiersApp)));
});

// Prise en charge des requêtes
self.addEventListener('fetch', e => {

  // DEBUG
  // var chemin = String(String(e.request.url).split("/").slice(-1));
  // var cheminSansExtension = chemin.split(".").length >= 1 ? String(chemin.split(".")[0]) : chemin;
  // console.log("[SW DEBUG] Chemin (sans extension) : " + cheminSansExtension);
  // if(pagesSite.includes(cheminSansExtension)) {
  //   console.log("[DANS LA LISTE] Inclus !");
  // } else {
  //   console.log("[DANS LA LISTE] pas dedans");
  // }
  // DEBUG

  // On va chercher le contenu en cache avec le SW
  e.respondWith(caches.match(e.request).then(response => {
    console.log("[service Worker] : va chercher la ressource " + e.request.url);
    return response || fetch(e.request)
  }));
});

// Suppression des anciens caches
this.addEventListener('activate', function(event) {
  var cacheWhitelist = [cacheName];
  event.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (cacheWhitelist.indexOf(key) === -1) {
          return caches.delete(key);
        }
      }));
    })
  );
});

/* ***** Exemple FETCH Master *****
self.addEventListener('fetch', e => {
  e.respondWith(caches.match(e.request).then(response => response || fetch(e.request)));
});
*/

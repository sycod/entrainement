export default class Signal {

    constructor(nom = false, duree = false, son = false, message = false, groupe = false) {
        this.nom = nom;
        this.duree = duree;
        this.son = son;
        this.message = message ? message : "";
        this.groupe = groupe ? groupe : 0;
    }
}

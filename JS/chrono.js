export default class Chrono {

    constructor() {
        this.intro = 0;     // durée de l'intro : remettre à 3 après tests
        this.compteur = 0;
        this.encours = false;
        this.min = 0;
        this.sec = 0;
        this.fin = false;
        this.sonFin = true;
        this.audio = "";
        this.prochainChrono = [];

        // ******************************************************************************
        // TODO
        // 
        // faire init() avec paramètres liste d'objets
        // 
        // supprimer cycles (plus dans le constructeur)
        // 
        // 
        // 
        // créer liste this.prochainChrono
        // gérer son fin uniquement (garder état load / pause / reprise)
        //     ► this.audio = référence du son choisi
        // tri classes & ID pr CSS & JS
        // 
        // ******************************************************************************
    }

    affichage() {
        var template = `
        <audio id="ouais"><source src="/audio/POC_ouais.mp3" type="audio/mpeg"></audio>
        <audio id="laser"><source src="/audio/POC_laser.mp3" type="audio/mpeg"></audio>
        <audio id="voila"><source src="/audio/POC_et_voila.mp3" type="audio/mpeg"></audio>

        <div id="intro" class="intro" hidden>
            <span id="decompte"></span>
        </div>
        
        <div id="affichCoherCard" class="inspiCoherCard" hidden>

            <div id="timerEnsemble">
                <span id="minutes"></span>:<span id="secondes"></span>
            </div>

            <div id="cycle">
                <span id="cycle"></span>
            </div>

            <div id="phase">
                <span id="phase"></span>
            </div>

        </div>
        
        <button id="boutonDepart" title="Départ" onclick="ENSEMBLE.decompteIntro()"><i class="fas fa-play"></i></button>
        <button id="boutonPause" title="Pause" class="inspiBouton" hidden onclick="ENSEMBLE.pause()"><i class="fas fa-pause"></i></button>
        <button id="boutonReprise" title="Reprise" hidden onclick="ENSEMBLE.reprise()"><i class="fas fa-play"></i></button>
        <button id="boutonRecommencer" title="Recommencer" hidden onclick="ENSEMBLE.recommencer()"><i class="fas fa-undo-alt"></i></button>
        `;

        document.getElementById("app").innerHTML = template;
    }

    // ************ FONCTIONS D'INTRO (DÉCOMPTE) ************
    decompteIntro() {
        if (this.intro == 0) {
            document.getElementById("boutonDepart").hidden = true;
            this.depart();
        } else {
            document.getElementById("boutonDepart").hidden = true;
            document.getElementById("decompte").innerHTML = "...";
            document.getElementById("intro").hidden = false;
        
            this.compteur = Number(this.intro);
            this.encours = setInterval(this.decompte.bind(this), 1000);
        }
    }
    
    decompte() {
        if (this.compteur > 0) {
            document.getElementById("intro").hidden = false;
            document.getElementById("decompte").innerHTML = this.compteur + "...";
            this.compteur --;
        } else {
            clearInterval(this.encours);
            this.encours = false;
            this.compteur = 0;
            document.getElementById("intro").hidden = true;
            this.depart();
        }
    }
    // ******************************************************
    
    depart() {
        document.getElementById("affichCoherCard").hidden = false;
        document.getElementById("affichCoherCard").classList.add("affichCoherCard");
        document.getElementById("boutonPause").hidden = false;
        
        document.getElementById("minutes").innerHTML = "0" + this.min;
        document.getElementById("secondes").innerHTML = "0" + this.sec;
        document.getElementById("cycle").innerHTML = "Cycle " + this.cycle + "/30";
        
        this.affichPhase();
        if(this.sonInspi) this.sonPhase("load");
        this.encours = setInterval(this.timer.bind(this), 1000);
    }

    timer() {
        if (this.compteur == 9 && this.cycle == 30) {
            this.finExercice();
        } else if (this.compteur == 4) {
            this.phaseChgt();
            this.compteur ++;
        } else if (this.compteur == 9) {
            this.phaseChgt();
            this.cycle ++;
            this.compteur = 0;
            document.getElementById("cycle").innerHTML = "Cycle " + this.cycle + "/30";
        } else {
            this.compteur ++;
        }

        if(this.sec == 59) {this.min ++; this.sec = 0} else {this.sec ++;}
        document.getElementById("minutes").innerHTML = (this.min < 10 ? "0" + this.min : this.min);
        document.getElementById("secondes").innerHTML = (this.sec < 10 ? "0" + this.sec : this.sec);
    }

    pause() {
        clearInterval(this.encours);
        this.encours = false;

        if(this.sonInspi || this.sonExpi) this.sonPhase("pause");
        
        document.getElementById("boutonPause").hidden = true;
        document.getElementById("boutonReprise").hidden = false;
        document.getElementById("boutonRecommencer").hidden = false;
    }

    reprise() {
        document.getElementById("boutonRecommencer").hidden = true;
        document.getElementById("boutonReprise").hidden = true;
        document.getElementById("boutonPause").hidden = false;

        if(this.sonInspi || this.sonExpi) this.sonPhase("reprise");

        this.encours = setInterval(this.timer.bind(this), 1000);
    }

    recommencer() {
        this.min = 0;
        this.sec = 0;
        this.compteur = 0;
        this.cycle = 1;
        this.phase = "Inspiration";

        document.getElementById("affichCoherCard").classList.remove("affichCoherCard");
        document.getElementById("affichCoherCard").hidden = true;
        document.getElementById("boutonRecommencer").hidden = true;
        document.getElementById("boutonReprise").hidden = true;
        document.getElementById("boutonDepart").hidden = false;
    }
    
    phaseChgt() {
        if (this.phase == "Inspiration") {
            this.phase = "Expiration";
        } else {
            this.phase = "Inspiration";
        }
        this.affichPhase();
    }

    affichPhase() {
        if (this.phase == "Inspiration") {
            document.getElementById("affichCoherCard").classList.remove("expiCoherCard");
            document.getElementById("affichCoherCard").classList.add("inspiCoherCard");
            document.getElementById("boutonPause").classList.remove("expiBouton");
            document.getElementById("boutonPause").classList.add("inspiBouton");
            
            if(this.sonInspi) this.sonPhase("load");

        } else {
            document.getElementById("affichCoherCard").classList.remove("inspiCoherCard");
            document.getElementById("affichCoherCard").classList.add("expiCoherCard");
            document.getElementById("boutonPause").classList.remove("inspiBouton");
            document.getElementById("boutonPause").classList.add("expiBouton");
            
            if(this.sonExpi) this.sonPhase("load");
        }

        document.getElementById("phase").innerHTML = this.phase;
    }

    son(etat) {
        var son = this.phase == "Inspiration" ? document.getElementById("inspi") : document.getElementById("expi");
        switch (etat) {
            case "load":
                son.autoplay = true;
                son.load();
                break;
            case "pause":
                son.pause();
                break;
            case "reprise":
                son.play();
                break;
        }
    }

    finExercice() {
        clearInterval(this.encours);
        this.encours = false;

        document.getElementById("affichCoherCard").classList.remove("affichCoherCard");
        document.getElementById("affichCoherCard").hidden = true;
        document.getElementById("boutonPause").hidden = true;
        document.getElementById("boutonRecommencer").hidden = false;
        document.getElementById("boutonReprise").hidden = true;

        document.getElementById("decompte").innerHTML = "Bravo ! Exercice terminé :)";
        document.getElementById("intro").classList.add("outro");
        document.getElementById("intro").classList.remove("intro");
        document.getElementById("intro").hidden = false;

        var son = document.getElementById("sonFin");
        son.autoplay = true;
        son.load();
    }

}

export default class Entrainement {

    constructor() {

        // data
        this.cacheVersion = "goodtimes-v1.1.0";
        this.jsonInit = "../data/init.json";
        this.jsonProfil = "profilEntrainement";
        
        // config de l'ensemble
        this.maximum = 0;

        // moteur
        this.signal = {};
        this.encours = false;
        this.tpsTotal = 0;
        this.tpsSignal = 0;

        this.titre = "";
        this.icone = "";
        this.dureeIntro = 0;
        this.aRebours = false;
        this.modeChrono = false;
        this.ensemble = [];

        // TODO faire marcher ça
        document.getElementById("boutonDepart").addEventListener("click", this.decompteIntro);
        document.getElementById("boutonPause").addEventListener("click", this.pause);
        document.getElementById("boutonReprise").addEventListener("click", this.reprise);
        document.getElementById("boutonRecommencer").addEventListener("click", this.recommencer);
        document.getElementById("bontonReinitialiser").addEventListener("click", this.reinitialiser);
        document.getElementById("reglagesBouton").addEventListener("click", this.affichReglages.bind(this));

        this.initialisation = function() {
            // Initialisation des valeurs globales...
            fetch(this.jsonInit).then(r => r.json().then(json => {
                this.maximum = json.entrainement.maximum;
            }))
            // ... puis initialisation du profil
            .then(this.chargerDonnees())
            .then(this.affichExercices());
        
        }
    
    }
    
    chargerDonnees() {
        if (typeof localStorage!='undefined' && localStorage.getItem(this.jsonProfil)) {
            var p = JSON.parse(localStorage.getItem(this.jsonProfil));
            this.titre = p.titre;
            this.icone = p.icone;
            this.dureeIntro = p.dureeIntro;
            this.aRebours = p.aRebours;
            this.modeChrono = p.modeChrono;
            p.ensemble.forEach(e => { this.ensemble.push(e); });
            this.signal = this.ensemble[0];
        } else {
            fetch(this.jsonInit).then(r => r.json()).then(json => {
                var entr = json.entrainement;
                var ens = entr.ensemble;
                this.titre = entr.titre;
                this.icone = entr.icone;
                this.dureeIntro = entr.dureeIntro;
                this.aRebours = entr.aRebours;
                this.modeChrono = entr.modeChrono;
                ens.forEach(e => { this.ensemble.push(e); });
                this.signal = this.ensemble[0];
                this.affichExercices();
            });
            if (typeof localStorage == 'undefined') {
                alert("Attention : <i>localStorage</i> n'est pas supporté,<br>vous ne pouvez pas charger les paramètres personnalisés.<br><br>Mettez à jour votre navigateur<br>pour avoir toutes les fonctionnalités.");
            }
            this.affichExercices();
        }
    }

    affichExercices () {
        var liste = "";
        var l = document.getElementById("listeExercices");

        this.ensemble.forEach(e => {
            liste += `<div class="exercice">` + e.nom + ` • ` + e.duree + `"</div>`
        });
        l.innerHTML = liste;
    }

    affichDuree(duree) {
        if (duree < 60) { return duree < 10 ? `0${duree}"` : `${duree}"`; }
        else { return `${Math.trunc(duree/60)}'${(duree % 60 < 10) ? "0" : ""}${duree % 60}"`; }
    }

    decompteIntro() {
        document.getElementById("listeExercices").setAttribute("hidden", "");
        if (this.dureeIntro == 0) {
            document.getElementById("boutonDepart").classList.add("hidden");
            this.depart();
        } else {
            document.getElementById("boutonDepart").classList.add("hidden");
            document.getElementById("decompte").innerHTML = "...";
            document.getElementById("intro").classList.remove("hidden");
        
            this.compteur = Number(this.dureeIntro);

            function decompte() {
                if (this.compteur > 0) {
                    document.getElementById("intro").classList.remove("hidden");
                    document.getElementById("decompte").innerHTML = this.compteur + "...";
                    this.compteur --;
                } else {
                    clearInterval(this.encours);
                    this.encours = false;
                    this.compteur = 0;
                    document.getElementById("intro").classList.add("hidden");
                    this.depart();
                }
            }

            this.encours = setInterval(decompte, 1000);
        }
    }
    
    depart() {
        document.getElementById("boutonDepart").classList.add("hidden");
        document.getElementById("affichTempsTotal").removeAttribute("hidden");
        document.getElementById("boutonPause").classList.remove("hidden");
        
        document.getElementById("tempsTotal").innerHTML = this.affichDuree(this.tpsTotal);
        
        document.getElementById("tempsSignal").innerHTML = this.aRebours ? this.affichDuree(this.signal.duree) : this.affichDuree(this.tpsSignal);
        
        document.getElementById("nomSignal").innerHTML = this.signal.nom;                    
        document.getElementById("affichSignal").removeAttribute("hidden");
        
        this.encours = setInterval(this.timer.bind(this), 1000);
    }

    timer() {
        this.tpsTotal ++;
        document.getElementById("tempsTotal").innerHTML = this.affichDuree(this.tpsTotal);
        
        if (this.tpsSignal < this.signal.duree -1) { this.tpsSignal ++; }
        else {
            var i = this.ensemble.indexOf(this.signal);                    
            this.tpsSignal = 0;
            this.sonSignal("load");
            i >= (this.ensemble.length -1) ? this.finEnsemble() : this.signal = this.ensemble[i+1];
        }
        
        document.getElementById("tempsSignal").innerHTML = this.aRebours ? this.affichDuree(this.signal.duree - this.tpsSignal) : this.affichDuree(this.tpsSignal);
        document.getElementById("nomSignal").innerHTML = this.signal.nom;
    }

    pause() {
        clearInterval(this.encours);
        this.encours = false;

        this.sonSignal("pause");

        document.getElementById("boutonPause").classList.add("hidden");
        document.getElementById("boutonReprise").classList.remove("hidden");
        document.getElementById("boutonRecommencer").classList.remove("hidden");
    }
    
    reprise() {
        document.getElementById("boutonRecommencer").classList.add("hidden");
        document.getElementById("boutonReprise").classList.add("hidden");
        document.getElementById("boutonPause").classList.remove("hidden");

        this.encours = setInterval(this.timer.bind(this), 1000);
    }

    recommencer() {
        this.signal = this.ensemble[0];
        this.encours = false;
        this.tpsTotal = 0;
        this.tpsSignal = 0;

        this.initialisation();
    }
    
    sonSignal(etat) {
        if (this.signal.son) {
            var son = document.getElementById(this.signal.son);
            switch (etat) {
                case "load":
                    son.autoplay = true;
                    son.load();
                    break;
                case "pause":
                    son.pause();
                    break;
            }
        }
    }
    
    finEnsemble() {
        clearInterval(this.encours);
        this.encours = false;
        
        document.getElementById("affichSignal").setAttribute("hidden", "");
        document.getElementById("boutonPause").classList.add("hidden");
        document.getElementById("boutonReprise").classList.add("hidden");
        document.getElementById("boutonRecommencer").classList.remove("hidden");
        
        document.getElementById("decompte").innerHTML = "☼ Bravo !<br>Exercice terminé ☺";
        document.getElementById("intro").classList.add("outro");
        document.getElementById("intro").classList.remove("intro", "hidden");
        
    }

    affichReglages() {
        // blocs et boutons
        var intro = document.getElementById("intro");
        var blocReglages = document.getElementById("blocReglages");
        var boutonParametres = document.getElementById("reglagesBouton");
        var boutonDepart = document.getElementById("boutonDepart");
        var boutonPause = document.getElementById("boutonPause");
        var boutonReprise = document.getElementById("boutonReprise");
        var boutonRecommencer = document.getElementById("boutonRecommencer");
        var bontonReinitialiser = document.getElementById("bontonReinitialiser");
        
        // valeurs des formulaires
        var decompte = document.getElementById("formDecompte");
        var chronoPositif = document.getElementById("formChronoPositif");
        var chronoRebours = document.getElementById("formChronoRebours");
            
        // mettre valeurs par défaut
        chronoPositif.checked = !this.aRebours;
        chronoRebours.checked = this.aRebours;
        decompte.value = this.dureeIntro;
        this.affichSignaux();
        
        blocReglages.removeAttribute("hidden");
        boutonParametres.setAttribute("hidden", "");

        // mettre en pause l'appli + désactiver boutons
        if (!intro.classList.contains("hidden") || !boutonPause.classList.contains("hidden")) {
            clearInterval(this.encours);
            this.encours = false;
            this.sonSignal("pause");
        }
        
        boutonDepart.setAttribute("disabled", ""); boutonDepart.classList.add("desactive");
        boutonPause.setAttribute("disabled", ""); boutonPause.classList.add("desactive");
        boutonReprise.setAttribute("disabled", ""); boutonReprise.classList.add("desactive");
        boutonRecommencer.setAttribute("disabled", ""); boutonRecommencer.classList.add("desactive");
        if (typeof localStorage != 'undefined' && localStorage.getItem(this.jsonProfil)) {
            bontonReinitialiser.removeAttribute("hidden");
        }

        var formReglages = document.getElementById("formReglagesEnsemble");
        formReglages.addEventListener("submit", e => {
            var nbSignaux = this.ensemble.length;

            // TODO si this.modeChrono → supprimer :
            //     - affichage temps total
            //     - affichage titre
            
            if (!bontonReinitialiser.hasAttribute("hidden")) { bontonReinitialiser.setAttribute("hidden", ""); }
            blocReglages.setAttribute("hidden", "");

            // enregistrer les valeurs
            this.dureeIntro = Number(decompte.value);
            this.aRebours = chronoRebours.checked;
            this.ensemble = [];
            var i = 0;
            for (i = 0; i < nbSignaux; i++) {
                var s = {};
                s.nom = document.getElementById(`formSigNom${i}`).value;
                s.duree = Number(document.getElementById(`formSigDuree${i}`).value);
                s.son = document.getElementById(`formSigSon${i}`).value == "muet" ? false : document.getElementById(`formSigSon${i}`).value;
                this.ensemble.push(s);
            }

            var profilEntrainement = {};
            profilEntrainement.dureeIntro = this.dureeIntro;
            profilEntrainement.aRebours = this.aRebours;
            profilEntrainement.modeChrono = this.modeChrono;
            profilEntrainement.ensemble = [];
            this.ensemble.forEach(e => {
                profilEntrainement.ensemble.push({"nom" : e.nom, "duree" : e.duree, "son" : e.son})
            });

            if (typeof localStorage != 'undefined') {
                var p = JSON.stringify(profilEntrainement);
                localStorage.setItem(this.jsonProfil, p);
            } else {
                alert("Attention : <i>localStorage</i> n'est pas supporté,<br>vous ne pouvez pas enregistrer les paramètres personnalisés.<br><br>Mettez à jour votre navigateur<br>pour avoir toutes les fonctionnalités.");
            }

            this.recommencer();

        });

    }

    affichSignaux() {
        var modeleSig = `
        <div class="formSignaux" id="formSig$SIGNAL">
            <hr>
            <p><span style="white-space:nowrap">
                <label for="formSigNom$SIGNAL">Intitulé : </label>
                <input type="text" name="nomSig$SIGNAL" id="formSigNom$SIGNAL" class="formSigNom" minlength="1" maxlength="20" value="$SIG_NOM" required>
            </span></p>
            <p><span style="white-space:nowrap">
                <label for="formSigDuree$SIGNAL">Durée : </label>
                <input id="formSigDuree$SIGNAL" class="formSigDuree" type="number" min="1" max="86400" name="dureeSig$SIGNAL" value="$SIG_DUREE">"
            </span>
            <span style="white-space:nowrap">
                    &nbsp;&nbsp;
                <label for="formSigSon$SIGNAL">♪</label>
                <select id="formSigSon$SIGNAL" name="sonSig$SIGNAL">
                    <option value="muet">-- Silence --</option>
                    <option value="sonGongBase">Gong</option>
                    <option value="sonGongCourt">Gong court</option>
                    <option value="sonGongJapBase">Gong japonais</option>
                    <option value="sonGongJapBCourt">Gong japonais court</option>
                    <option value="sonGongJapAntiq">Gong antique</option>
                    <option value="sonGongJapACourt">Gong antique court</option>
                    <option value="sonClocheAigue">Cloche aigüe</option>
                    <option value="sonClocheMedium">cloche medium</option>
                    <option value="sonAtariBip">Atari Bip</option>
                    <option value="sonAtariCrash">Atari Crash</option>
                    <option value="sonAtariKirby">Atari Kirby</option>
                    <option value="sonAtariSaut">Atari Saut</option>
                    <option value="sonJLinspi">Inspiration</option>
                    <option value="sonJLexpi">Expiration</option>
                    <option value="sonPOCLaser">POC Laser</option>
                    <option value="sonPOCOuais">POC Ouais barbare</option>
                    <option value="sonPOCVoila">POC voilà nain</option>
                </select>
            </span></p>
            <button id="boutonAjoutSignal$SIGNAL" $DISABLED class="boutonSignal $GRISE" type="button" onclick="ensemble.ajoutSignal($SIGNAL)" title="Ajouter un signal"><svg class="icon"><use xlink:href="static/icons.svg#plus"/></svg></button>
            <button id="boutonSupprSignal$SIGNAL" class="boutonSignal" type="button" onclick="ensemble.supprSignal($SIGNAL)" title="Supprimer le signal"><svg class="icon"><use xlink:href="static/icons.svg#minus"/></svg></button>
        </div>
        `;
        var formSignaux = document.getElementById("formSignaux");
        formSignaux.innerHTML = "";
        var nbSignaux = this.ensemble.length;
        var max = this.maximum;
        this.ensemble.forEach(function (e, i) {
            var m = modeleSig.replaceAll("$SIGNAL", i)
            .replaceAll("$SIG_NOM", e.nom)
            .replaceAll("$SIG_DUREE", e.duree)
            .replaceAll(e.son + `"`, e.son + `" selected`)
            // Suppression du bouton ajout si > nb maximum de signaux
            .replaceAll("$DISABLED", nbSignaux >= max ? "disabled" : "")
            .replaceAll("$GRISE", nbSignaux >= max ? "desactive" : "");
            formSignaux.innerHTML += m;
        });
    }
    
    ajoutSignal(i) {
        // Limite du nombre de signaux
        // (sécu supp, bouton déjà désactivé)
        if (this.ensemble.length >= this.maximum) { return true;}

        var s = {};
        s.nom = document.getElementById(`formSigNom${i}`).value;
        s.duree = document.getElementById(`formSigDuree${i}`).value;
        s.son = document.getElementById(`formSigSon${i}`).value == "muet" ? false : document.getElementById(`formSigSon${i}`).value;

        // Mise à jour du signal copié selon le DOM
        this.ensemble.splice(i, 1, s);
        this.ensemble.splice(i+1, 0, s);
        this.affichSignaux();
    }
    
    supprSignal(i) {
        this.ensemble.splice(i, 1);
        if (this.ensemble.length == 0) {
            this.modeChrono = true;
            this.aRebours = false;
            this.dureeIntro = 0;
            document.getElementById("formDecompte").value = this.dureeIntro;
            this.ensemble.push({
                "nom": "Chronomètre simple",
                "duree": 86401,
                "son": "muet"
            });
        }
        this.affichSignaux();
    }

    reinitialiser() {
        if (typeof localStorage != 'undefined' && localStorage.getItem(this.jsonProfil)) {
            localStorage.removeItem(this.jsonProfil);
        }
        this.recommencer();
    }

};

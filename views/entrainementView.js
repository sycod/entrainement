import affichIndex from "./indexView.js";
import { APP_ENTRAINEMENT as TEMPLATE } from "../static/templates.js";
import Entrainement from "../JS/entrainement.js";


export default function() {
    const TITRE_H1 = "Entraînement";
        
    // chargement des templates et mise à jour de l'affichage
    document.getElementsByTagName("h1")[0].innerHTML = TITRE_H1;
    document.getElementsByTagName("app")[0].innerHTML = TEMPLATE;

    document.getElementsByClassName("retour")[0].removeAttribute("hidden");
    document.getElementsByClassName("aide")[0].removeAttribute("hidden");

    document.getElementById("retourAccueil").addEventListener("click", affichIndex);

    var entrainement = new Entrainement();
    entrainement.initialisation();
};

import { HEADER } from "../static/templates.js";
import { APP_INDEX } from "../static/templates.js";


const TITRE_H1 = "Good Times";


export default function affichage() {
    var header = HEADER.replace("$TITRE_H1", TITRE_H1);
    document.getElementsByTagName("header")[0].innerHTML = header;
    document.getElementsByTagName("app")[0].innerHTML = APP_INDEX;
};
